This is a study project named Notebook. It includes a console user interface with which user can record contacts to the database, find the necessary contacts by pattern, send contacts by email, view all contacts, change and remove them. 

There are two approaches for working with the database (JDBC and Hibernate Framework).

Pattern search is performed using the Karp Rabin algorithm.

I use my own implementation of HashMap and Pair.

The project is partially covered by unit tests using mock-objects.