package unit_test;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import patternsearch.ITextPatternSearch;
import patternsearch.KarpRabinPatternSearch;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestKarpRabinPatternSearch {
    private ITextPatternSearch karpRabinPatternSearch;

    @Before
    public void initTest() {
        karpRabinPatternSearch = new KarpRabinPatternSearch();
    }

    @After
    public void afterTest() {
        karpRabinPatternSearch = null;
    }

    @Test
    public void findPatternTest() {
        List textContainsPattern = karpRabinPatternSearch.findPattern("mum".toCharArray(),
                "Hey Mum,momummamumUmU".toCharArray());
        assertEquals(2, textContainsPattern.size());
        textContainsPattern = karpRabinPatternSearch.findPattern("Test".toCharArray(), "testTesttesT".toCharArray());
        assertEquals(1, textContainsPattern.size());
        textContainsPattern = karpRabinPatternSearch.findPattern("Vakj".toCharArray(), "VajdkfiVtj".toCharArray());
        assertEquals(0, textContainsPattern.size());
        textContainsPattern = karpRabinPatternSearch.findPattern("".toCharArray(), "VajdkfiVtj".toCharArray());
        assertEquals(0, textContainsPattern.size());
        textContainsPattern = karpRabinPatternSearch.findPattern("Spb".toCharArray(), "".toCharArray());
        assertEquals(0, textContainsPattern.size());
        textContainsPattern = karpRabinPatternSearch.findPattern("ss".toCharArray(), "assssss".toCharArray());
        assertEquals(5, textContainsPattern.size());
    }

    @Test
    public void findPatternLowerCaseTest() {
        List textContainsPattern = karpRabinPatternSearch.findPattern("mum".toLowerCase().toCharArray(),
                "Hey Mum,momummamumUmU".toLowerCase().toCharArray());
        assertEquals(4, textContainsPattern.size());
        textContainsPattern = karpRabinPatternSearch.findPattern("Test".toLowerCase().toCharArray(),
                "testTesttesT".toLowerCase().toCharArray());
        assertEquals(3, textContainsPattern.size());
        textContainsPattern = karpRabinPatternSearch.findPattern("Vakj".toLowerCase().toCharArray(),
                "VajdkfiVtj".toLowerCase().toCharArray());
        assertEquals(0, textContainsPattern.size());
        textContainsPattern = karpRabinPatternSearch.findPattern("".toLowerCase().toCharArray(),
                "VajdkfiVtj".toLowerCase().toCharArray());
        assertEquals(0, textContainsPattern.size());
        textContainsPattern = karpRabinPatternSearch.findPattern("Spb".toLowerCase().toCharArray(),
                "".toLowerCase().toCharArray());
        assertEquals(0, textContainsPattern.size());
        textContainsPattern = karpRabinPatternSearch.findPattern("SS".toLowerCase().toCharArray(),
                "assssss".toLowerCase().toCharArray());
        assertEquals(5, textContainsPattern.size());
    }
}
