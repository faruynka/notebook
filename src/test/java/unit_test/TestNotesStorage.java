package unit_test;

import myhashmap.IMap;
import notesstorage.Note;
import notesstorage.NoteObject;
import notesstorage.NotesStorage;
import patternsearch.ITextPatternSearch;
import org.junit.*;

import static org.easymock.EasyMock.*;
import static org.junit.Assert.*;

public class TestNotesStorage {
    private NotesStorage storage;
    private IMap<String, Note> mapMock;
    private ITextPatternSearch patternSearchMock;
    private Note exampleNote = new Note("03293", "483748", "coca", "Spb",
            "vasya.pupkin@gmail.com");

    @Before
    public void initTest() {
        mapMock = createNiceMock(IMap.class);
        patternSearchMock = createNiceMock(ITextPatternSearch.class);
        storage = new NotesStorage(mapMock, patternSearchMock);
    }

    @After
    public void afterTest() {
        mapMock = null;
        patternSearchMock = null;
        storage = null;
    }

    @Test
    public void testAdd() {
        expect(mapMock.insert(eq("Nastya"), same(exampleNote))).andReturn(true);
        expect(mapMock.insert(eq("Vasya"), same(exampleNote))).andReturn(false);
        replay(mapMock);
        assertTrue(storage.add("Nastya", exampleNote));
        assertFalse(storage.add("Vasya", exampleNote));
        verify(mapMock);
    }

    @Test
    public void testRemove() {
        expect(mapMock.remove(eq("Nastya"))).andReturn(true);
        expect(mapMock.remove(eq("Vasya"))).andReturn(false);
        replay(mapMock);
        assertTrue(storage.remove("Nastya"));
        assertFalse(storage.remove("Vasya"));
        verify(mapMock);
    }

    @Test
    public void testChange() {
        expect(mapMock.find(eq("Nastya"))).andReturn(exampleNote);
        expect(mapMock.remove(eq("Nastya"))).andReturn(true);
        expect(mapMock.insert(eq("Nastya"), isA(Note.class))).andReturn(true);
        expect(mapMock.find(eq("Vasya"))).andReturn(null);
        replay(mapMock);
        assertTrue(storage.change("Nastya", NoteObject.ADDRESS, "kjsdakd"));
        assertFalse(storage.change("Vasya", NoteObject.COMPANY, "cola"));
        verify(mapMock);
    }

    @Test
    public void testFind() {
        expect(mapMock.find(eq("Nastya"))).andReturn(exampleNote);
        expect(mapMock.find(eq("Vasya"))).andReturn(null);
        replay(mapMock);
        assertEquals(exampleNote, storage.find("Nastya"));
        assertNull(storage.find("Vasya"));
        verify(mapMock);
    }
}
