package unit_test;

import myhashmap.IMapIterator;
import myhashmap.MyHashMap;
import org.junit.*;

import static org.junit.Assert.*;

public class TestHashMap {
private MyHashMap<String, String> myHashMap;

    @Before
    public void initTest() {
        myHashMap = new MyHashMap();
    }

    @After
    public void afterTest() {
        myHashMap = null;
    }

    @Test
    public void testCapacity() {
        assertEquals(8, myHashMap.capacity());
        myHashMap.insert("Leo", "Spb");
        assertEquals(8, myHashMap.capacity());
        myHashMap.insert("Leo", "Spb");
        myHashMap.insert("Petya", "Spb");
        myHashMap.insert("Vasya", "Neapol");
        myHashMap.insert("Tanya", "Neapol");
        myHashMap.insert("Lena", "Neapol");
        myHashMap.insert("Sveta", "Neapol");
        assertEquals(8, myHashMap.capacity());
        myHashMap.insert("Misha", "Neapol");
        assertEquals(16, myHashMap.capacity());
        myHashMap.insert("Valera", "Neapol");
        myHashMap.insert("Kristina", "Neapol");
        myHashMap.insert("Kira", "Neapol");
        assertEquals(16, myHashMap.capacity());
    }

    @Test
    public void testSize() {
        myHashMap.insert("Leo", "Spb");
        assertEquals(1, myHashMap.size());
        myHashMap.insert("Leo", "Spb");
        assertEquals(1, myHashMap.size());
        myHashMap.insert("Kira", "Neapol");
        assertEquals(2, myHashMap.size());
    }

    @Test
    public void testClear() {
        myHashMap.insert("Leo", "Spb");
        myHashMap.insert("Petya", "Spb");
        myHashMap.insert("Vasya", "Neapol");
        myHashMap.insert("Tanya", "Neapol");
        myHashMap.insert("Lena", "Neapol");
        myHashMap.insert("Sveta", "Neapol");
        myHashMap.insert("Misha", "Neapol");
        myHashMap.insert("Valera", "Neapol");
        myHashMap.insert("Kristina", "Neapol");
        myHashMap.insert("Kira", "Neapol");
        myHashMap.clear();
        assertEquals(0, myHashMap.size());
        assertEquals(8, myHashMap.capacity());
    }

    @Test
    public void testInsert() {
        myHashMap.insert("Leo", "Spb");
        myHashMap.insert("Petya", "Moscow");
        myHashMap.insert("Vasya", "Neapol");
        myHashMap.insert("Tanya", "Soligorsk");
        myHashMap.insert("Tanya", "Minsk");
        myHashMap.insert("Lena", "Minsk");
        myHashMap.insert("Sveta", "Turkey");
        myHashMap.insert("Misha", "Viena");
        myHashMap.insert("Valera", "Authens");
        myHashMap.insert("Valera", "Rome");
        myHashMap.insert("Kristina", "Rome");
        myHashMap.insert("Kristina", "Soligorsk");
        myHashMap.insert("Kira", "Paris");
        assertEquals("Spb", myHashMap.find("Leo"));
        assertEquals("Moscow", myHashMap.find("Petya"));
        assertEquals("Neapol", myHashMap.find("Vasya"));
        assertEquals("Soligorsk", myHashMap.find("Tanya"));
        assertEquals("Minsk", myHashMap.find("Lena"));
        assertEquals("Turkey", myHashMap.find("Sveta"));
        assertEquals("Viena", myHashMap.find("Misha"));
        assertEquals("Authens", myHashMap.find("Valera"));
        assertEquals("Rome", myHashMap.find("Kristina"));
        assertEquals("Paris", myHashMap.find("Kira"));
    }

    @Test
    public void testRemove() {
        myHashMap.insert("Leo", "Spb");
        myHashMap.insert("Petya", "Moscow");
        assertFalse(myHashMap.remove("Vasya"));
        assertFalse(myHashMap.remove("Tanya"));
        assertTrue(myHashMap.remove("Leo"));
        assertNull(myHashMap.find("Leo"));
        assertTrue(myHashMap.remove("Petya"));
        assertNull(myHashMap.find("Petya"));
    }

    @Test
    public void testFind() {
        myHashMap.insert("Leo", "Spb");
        myHashMap.insert("Petya", "Moscow");
        myHashMap.insert("Vasya", "Neapol");
        myHashMap.insert("Tanya", "Soligorsk");
        myHashMap.remove("Tanya");
        myHashMap.insert("Lena", "Minsk");
        assertNotNull(myHashMap.find("Leo"));
        assertNotNull(myHashMap.find("Petya"));
        assertNotNull(myHashMap.find("Vasya"));
        assertNotNull(myHashMap.find("Lena"));
        assertNull(myHashMap.find("Kira"));
        assertNull(myHashMap.find("Kristina"));
        assertNull(myHashMap.find("Valera"));
        assertNull(myHashMap.find("Tanya"));
    }

    @Test
    public void testIterator() {
        myHashMap.insert("Leo", "Spb");
        myHashMap.insert("Petya", "Moscow");
        myHashMap.insert("Vasya", "Neapol");
        myHashMap.insert("Tanya", "Soligorsk");
        myHashMap.insert("Lena", "Minsk");
        IMapIterator iterator = myHashMap.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            i++;
            assertNotNull(iterator.next());
        }
        assertEquals(5, i);
    }
}
