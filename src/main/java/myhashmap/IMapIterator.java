package myhashmap;

public interface IMapIterator {
    boolean hasNext();
    MyPair next();
}
