package myhashmap;

public class MyHashMap<T, G> implements IMap<T, G> {
    private HashMapNode[] array;
    private int size;

    public MyHashMap() {
        setArray();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public int capacity() {
        return array.length;
    }

    @Override
    public void clear() {
        setArray();
    }

    @Override
    public boolean insert(T key, G value) {
        int index = getIndex(key);
        if (findAlgorithm(key, index) != null) {
            return false;
        }
        HashMapNode elements = array[index];
        elements = new HashMapNode(key, value, elements);
        size++;
        array[index] = elements;
        if (size > (int) (array.length * 0.8)) {
            extendMap();
        }
        return true;
    }

    @Override
    public boolean remove(T key) {
        int index = getIndex(key);
        HashMapNode elements = array[index];
        if (elements == null) {
            return false;
        }
        if (elements.key.equals(key)) {
            if (elements.hasNext()) {
                elements = elements.next;
            } else {
                elements = null;
            }
            size--;
            array[index] = elements;
            return true;
        }
        HashMapNode prev = elements;
        HashMapNode next = elements.next;
        while (prev.hasNext()) {
            if (next.key.equals(key)) {
                prev.next = next.next;
                size--;
                return true;
            }
            prev = next;
            next = prev.next;
        }
        return false;
    }

    @Override
    public G find(T key) {
        int index = getIndex(key);
        return findAlgorithm(key, index);
    }

    @Override
    public IMapIterator iterator() {
        return new HashMapIterator();
    }

    private int getIndex(T key) {
        return ((key.hashCode() % array.length) + array.length) % array.length;
    }

    private void extendMap() {
        size = 0;
        HashMapNode[] oldArray = array;
        this.array = new HashMapNode[oldArray.length * 2];
        for (int i = 0; i < oldArray.length; i++) {
            HashMapNode<T, G> node = oldArray[i];
            if (node != null) {
                insert(node.key, node.value);
                while (node.hasNext()) {
                    node = node.next;
                    insert(node.key, node.value);
                }
            }
        }
    }

    private G findAlgorithm(T key, int hash) {
        HashMapNode elements = array[hash];
        if (elements == null) {
            return null;

        } else if (elements.key.equals(key)) {
            return (G) elements.value;
        }
        HashMapNode nextElement = new HashMapNode(elements.key, elements.value, elements.next);
        while (nextElement.hasNext()) {
            if (nextElement.next.key.equals(key)) {
                return (G) nextElement.next.value;

            }
            nextElement = nextElement.next;
        }
        return null;
    }

    private void setArray() {
        this.array = new HashMapNode[8];
        size = 0;
    }

    class HashMapIterator implements IMapIterator{
        private int i = 0;
        private HashMapNode<T, G> node = array[i];

        HashMapIterator() {
            nextChain();
        }

        @Override
        public boolean hasNext() {
            return i < array.length - 1 || node != null;
        }

        @Override
        public MyPair next() {
            if (hasNext()) {
                MyPair res = new MyPair(node.key, node.value);
                if (node != null) {
                    node = node.next;
                }
                if (node == null) {
                    nextChain();
                }
                return res;
            }
            return null;
        }

        private void nextChain() {
            while (hasNext()) {
                if (node != null) {
                    break;
                }
                ++i;
                node = array[i];
            }
        }
    }

    private class HashMapNode<T, G> {
        private T key;
        private G value;
        private HashMapNode next;

        public HashMapNode(T key, G value, HashMapNode next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }

        private boolean hasNext() {
            return next != null;
        }
    }
}