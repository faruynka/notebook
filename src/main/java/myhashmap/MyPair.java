package myhashmap;

import java.util.Objects;

public class MyPair<T, G> {
    private T value1;
    private G value2;

    public MyPair(T value1, G value2) {
        this.value1 = value1;
        this.value2 = value2;
    }

    public T getFirst() {
        return value1;
    }

    public G getSecond() {
        return value2;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        MyPair<T, G> other = (MyPair) obj;
        return Objects.equals(value1, other.value1) && Objects.equals(value2, other.value2);
    }

    @Override
    public int hashCode() {
        return this.value1.hashCode() + this.value2.hashCode();
    }

    public static MyPair of(Object value1, Object value2) {
        return new MyPair(value1, value2);
    }
}
