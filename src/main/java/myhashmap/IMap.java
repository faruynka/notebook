package myhashmap;

public interface IMap<T, G> {
    int size();

    int capacity();

    void clear();

    boolean insert(T key, G value);

    boolean remove(T key);

    G find(T key);

    IMapIterator iterator();
}