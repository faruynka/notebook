package database;

import myhashmap.IMap;
import notesstorage.Note;
import notesstorage.NoteObject;

public interface IDataBaseService {
    IMap<String, Note> importData();

    void closeConnection();

    void addContactIntoDataBase(String name, Note note);

    void deleteContactInDataBase(String name);

    void changeContactInDataBase(String name, NoteObject noteObject, String changes);
}
