package database.jdbcservice;

import database.IDataBaseService;
import myhashmap.IMap;
import myhashmap.MyHashMap;
import notesstorage.Note;
import notesstorage.NoteObject;

import java.sql.*;

public class JDBCService implements IDataBaseService {
    private Connection connection;
    private Statement statement;

    public JDBCService() {
        connect();
        try {
            statement = connection.createStatement();
            createTablesIfNotExists();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        createTablesIfNotExists();
    }

    @Override
    public void closeConnection() {
        try {
            statement.close();
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public IMap<String, Note> importData() {
        String quary = "SELECT p.name, p.mobile_phone, p.email, c.company, h.home_phone, h.address" +
                " FROM personal_data AS p INNER JOIN company AS c ON p.company_id = c.id INNER JOIN home_data AS h " +
                "ON p.home_data_id = h.id";
        IMap<String, Note> notebook = new MyHashMap();
        try (ResultSet data = statement.executeQuery(quary)) {
            while (data.next()) {
                notebook.insert(data.getString("name"), new Note(data.getString("mobile_phone"),
                        data.getString("home_phone"), data.getString("company"), data.getString("address"),
                        data.getString("email")));
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return notebook;
    }

    @Override
    public void addContactIntoDataBase(String name, Note note) {
        try {
            int companyId;
            String companyQuary = "INSERT INTO company (company) VALUES ('" + note.company() + "')";
            statement.executeUpdate(companyQuary);
            try (ResultSet rs = statement.executeQuery("SELECT last_insert_rowid()")) {
                rs.next();
                companyId = rs.getInt(1);
            }
            String homeDataQuary = "INSERT INTO home_data (home_phone, address) VALUES ('" + note.homeNumber()
                    + "', '" + note.address() + "')";
            statement.executeUpdate(homeDataQuary);
            try (ResultSet rs = statement.executeQuery("SELECT last_insert_rowid()")) {
                rs.next();
                int homeDataId = rs.getInt(1);
                String personalDataQuary = "INSERT INTO personal_data (name, mobile_phone, email, company_id," +
                        " home_data_id) VALUES ('" + name + "', '" + note.number() + "', '"
                        + note.email() + "', " + companyId + ", " + homeDataId + ")";
                statement.executeUpdate(personalDataQuary);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void deleteContactInDataBase(String name) {
        try {
            int companyId;
            try (ResultSet rs = statement.executeQuery("SELECT company_id FROM personal_data WHERE name = '"
                    + name + "'")) {
                rs.next();
                companyId = rs.getInt(1);
            }
            try (ResultSet rs = statement.executeQuery("SELECT home_data_id FROM personal_data WHERE name = '"
                    + name + "'")) {
                rs.next();
                int homeDataId = rs.getInt(1);
                String personalDataQuary = "DELETE FROM personal_data WHERE name = '" + name + "'";
                statement.executeUpdate(personalDataQuary);
                String companyDataQuary = "DELETE FROM company WHERE id = '" + companyId + "'";
                statement.executeUpdate(companyDataQuary);
                String homeDataQuary = "DELETE FROM home_data WHERE id = '" + homeDataId + "'";
                statement.executeUpdate(homeDataQuary);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void changeContactInDataBase(String name, NoteObject noteObject, String changes) {
        try {
            String quary = null;
            if (noteObject.equals(NoteObject.NUMBER)) {
                quary = "UPDATE personal_data SET mobile_phone = '" + changes + "' WHERE name = '" + name + "'";
            } else if (noteObject.equals(NoteObject.EMAIL)) {
                quary = "UPDATE personal_data SET email = '" + changes + "' WHERE name = '" + name + "'";
            } else if (noteObject.equals(NoteObject.COMPANY)) {
                try (ResultSet rs = statement.executeQuery("SELECT company_id FROM personal_data WHERE name = '"
                        + name + "'")) {
                    rs.next();
                    int companyId = rs.getInt(1);
                    quary = "UPDATE company SET company = '" + changes + "' WHERE id = '" + companyId + "'";
                }
            } else if (noteObject.equals(NoteObject.ADDRESS)) {
                try (ResultSet rs = statement.executeQuery("SELECT home_data_id FROM personal_data WHERE name = '"
                        + name + "'")) {
                    rs.next();
                    int homeDataId = rs.getInt(1);
                    quary = "UPDATE home_data SET address = '" + changes + "' WHERE id = '" + homeDataId + "'";
                }
            } else if (noteObject.equals(NoteObject.HOMENUMBER)) {
                try (ResultSet rs = statement.executeQuery("SELECT home_data_id FROM personal_data WHERE name = '"
                        + name + "'")) {
                    rs.next();
                    int homeDataId = rs.getInt(1);
                    quary = "UPDATE home_data SET home_phone = '" + changes + "' WHERE id = '" + homeDataId + "'";
                }
            }
            statement.executeUpdate(quary);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void connect() {
        try {
            Class.forName("org.sqlite.JDBC").newInstance();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:notebook.db");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    private void createTablesIfNotExists() {
        try {
            String swichForeignKEY = "PRAGMA foreign_keys=on";

            String firstTable = "CREATE TABLE IF NOT EXISTS personal_data " +

                    "(id INTEGER PRIMARY KEY AUTOINCREMENT," +

                    " name TEXT NOT NULL, " +

                    " mobile_phone TEXT NOT NULL," +

                    " email TEXT NOT NULL," +

                    "company_id INTEGER NOT NULL," +

                    "home_data_id INTEGER NOT NULL," +

                    "FOREIGN KEY (company_id) REFERENCES company(id)," +

                    "FOREIGN KEY (home_data_id) REFERENCES home_data(id))";

            String secondTable = "CREATE TABLE IF NOT EXISTS company " +

                    "(id INTEGER PRIMARY KEY AUTOINCREMENT," +

                    " company TEXT NOT NULL)";

            String thirdTable = "CREATE TABLE IF NOT EXISTS home_data" +

                    "(id INTEGER PRIMARY KEY AUTOINCREMENT," +

                    " home_phone TEXT NOT NULL," +

                    " address TEXT NOT NULL)";

            statement.executeUpdate(swichForeignKEY);
            statement.executeUpdate(firstTable);
            statement.executeUpdate(secondTable);
            statement.executeUpdate(thirdTable);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
