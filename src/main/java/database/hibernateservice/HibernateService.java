package database.hibernateservice;

import database.IDataBaseService;
import database.hibernateservice.dao.ClientsDAO;
import database.hibernateservice.dataSets.Company;
import database.hibernateservice.dataSets.HomeData;
import database.hibernateservice.dataSets.PersonalData;
import myhashmap.IMap;
import notesstorage.Note;
import notesstorage.NoteObject;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateService implements IDataBaseService {
    private SessionFactory sessionFactory;

    public HibernateService() {
        Configuration configuration = getSQLiteConfiguration();
        sessionFactory = createSessionFactory(configuration);
    }

    public IMap<String, Note> importData() {
        Session session = sessionFactory.openSession();
        ClientsDAO clientsDAO = new ClientsDAO(session);
        IMap<String, Note> importedData= clientsDAO.importData();
        session.close();
        return importedData;
    }

    public void closeConnection() {
        sessionFactory.close();
    }

    public void deleteContactInDataBase(String name) {
        Session session = sessionFactory.openSession();
        ClientsDAO clientsDAO = new ClientsDAO(session);
        clientsDAO.deleteContactInDataBase(name);
        session.close();
    }

    public void changeContactInDataBase(String name, NoteObject noteObject, String changes) {
        Session session = sessionFactory.openSession();
        ClientsDAO clientsDAO = new ClientsDAO(session);
        clientsDAO.changeContactInDataBase(name, noteObject, changes);
        session.close();
    }

    public void addContactIntoDataBase(String name, Note note) {
        Session session = sessionFactory.openSession();
        ClientsDAO clientsDAO = new ClientsDAO(session);
        clientsDAO.addContactIntoDataBase(name, note);
        session.close();
    }

    private Configuration getSQLiteConfiguration() {
        Configuration configuration = new Configuration();
        configuration.addAnnotatedClass(PersonalData.class);
        configuration.addAnnotatedClass(HomeData.class);
        configuration.addAnnotatedClass(Company.class)
                .setProperty("hibernate.dialect", "org.hibernate.dialect.SQLiteDialect")
                .setProperty("hibernate.connection.driver_class", "org.sqlite.JDBC")
                .setProperty("hibernate.connection.url", "jdbc:sqlite:notebook.db")
                .setProperty("hibernate.connection.pool_size", "1")
                .setProperty("hibernate.show_sql", "true")
                .setProperty("hibernate.format_sql", "true");
        return configuration;
    }

    private static SessionFactory createSessionFactory(Configuration configuration) {
        StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder();
        builder.applySettings(configuration.getProperties());
        ServiceRegistry serviceRegistry = builder.build();
        return configuration.buildSessionFactory(serviceRegistry);
    }
}
