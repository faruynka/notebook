package database.hibernateservice.dao;

import database.hibernateservice.dataSets.Company;
import database.hibernateservice.dataSets.HomeData;
import database.hibernateservice.dataSets.PersonalData;
import myhashmap.IMap;
import myhashmap.MyHashMap;
import notesstorage.Note;
import notesstorage.NoteObject;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class ClientsDAO {
    private Session session;

    public ClientsDAO(Session session) {
        this.session = session;
    }

    public IMap<String, Note> importData() {
        IMap<String, Note> result = new MyHashMap();
        Transaction transaction = session.beginTransaction();
        List<PersonalData> importedData = session.createQuery("from PersonalData").getResultList();
        for (PersonalData personalData : importedData) {
            result.insert(personalData.getName(), new Note(personalData.getMobile_phone(),
                    personalData.getHomeData().getHomePhone(), personalData.getCompany().getCompany(),
                    personalData.getHomeData().getAddress(), personalData.getEmail()));
        }
        transaction.commit();
        return result;
    }

    public void deleteContactInDataBase(String name) {
        Transaction transaction = session.beginTransaction();
        PersonalData personalData = getDataByName(name);
        Company company = personalData.getCompany();
        HomeData homeData = personalData.getHomeData();
        session.delete(personalData);
        session.delete(company);
        session.delete(homeData);
        transaction.commit();
    }

    public void changeContactInDataBase(String name, NoteObject noteObject, String changes) {
        PersonalData personalData = getDataByName(name);
        Transaction transaction = session.beginTransaction();
        if (noteObject.equals(NoteObject.NUMBER)) {
            personalData.setMobile_phone(changes);
        } else if (noteObject.equals(NoteObject.EMAIL)) {
            personalData.setEmail(changes);
        } else if (noteObject.equals(NoteObject.COMPANY)) {
            personalData.getCompany().setCompany(changes);
        } else if (noteObject.equals(NoteObject.ADDRESS)) {
            personalData.getHomeData().setAddress(changes);
        } else if (noteObject.equals(NoteObject.HOMENUMBER)) {
            personalData.getHomeData().setHomePhone(changes);
        }
        transaction.commit();
    }

    public void addContactIntoDataBase(String name, Note note) {
        Company company = new Company();
        company.setCompany(note.company());
        session.save(company);
        HomeData homeData = new HomeData();
        homeData.setAddress(note.address());
        homeData.setHomePhone(note.homeNumber());
        session.save(homeData);
        PersonalData personalData = new PersonalData(name, note.number(), note.email(), company, homeData);
        session.save(personalData);
    }

    private PersonalData getDataByName(String name) {
        Query personalDataQuary = session.createQuery("from PersonalData where name = :name")
                .setParameter("name", name);
        return (PersonalData) personalDataQuary.getResultList().get(0);
    }
}
