package database.hibernateservice.dataSets;

import javax.persistence.*;

@Entity
@Table(name = "home_data")
public class HomeData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "home_phone")
    private String homePhone;

    @Column(name = "address")
    private String address;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getHomePhone() {
        return homePhone;
    }

    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "HomeData{" +
                "id=" + id +
                ", homePhone='" + homePhone + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
