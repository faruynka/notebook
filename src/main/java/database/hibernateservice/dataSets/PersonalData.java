package database.hibernateservice.dataSets;

import javax.persistence.*;

@Entity
@Table(name = "personal_data")
public class PersonalData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "mobile_phone")
    private String mobile_phone;

    @Column(name = "email")
    private String email;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "company_id")
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "home_data_id")
    private HomeData homeData;

    public PersonalData() {
    }

    public PersonalData(String name, String mobile_phone, String email, Company company, HomeData homeData) {
        this.name = name;
        this.mobile_phone = mobile_phone;
        this.email = email;
        this.company = company;
        this.homeData = homeData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_phone() {
        return mobile_phone;
    }

    public void setMobile_phone(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public HomeData getHomeData() {
        return homeData;
    }

    public void setHomeData(HomeData homeData) {
        this.homeData = homeData;
    }

    @Override
    public String toString() {
        return "PersonalData{" +
                "id=" + id +
                ", firstName='" + name + '\'' +
                ", lastName='" + mobile_phone + '\'' +
                ", age=" + email +
                '}';
    }
}
