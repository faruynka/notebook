package notebook;

import java.util.List;
import java.util.Scanner;

class FindContactsByPattern implements ICommand {
    @Override
    public void execute(NoteBookApp app) {
        if (app.getStorage().getNoteBook().size() == 0) {
            System.out.println("Your notebook is empty. Choose first command to add contacts.");
            return;
        }
        Scanner scan = new Scanner(System.in);
        System.out.println("Write pattern");
        String pattern = scan.next();
        List<String> contacts = app.getStorage().findNotesWithPattern(pattern);
        for (int i = 0; i < contacts.size(); i++) {
            System.out.println(contacts.get(i));
        }
    }

    @Override
    public String name() {
        return "Find contact by pattern";
    }
}
