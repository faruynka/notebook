package notebook;

import notesstorage.NoteObject;

import java.util.InputMismatchException;
import java.util.Scanner;

class ChangeCommand implements ICommand {
    @Override
    public void execute(NoteBookApp app) throws InputMismatchException {
        if (app.getStorage().getNoteBook().size() == 0) {
            System.out.println("Your notebook is empty. Choose first command to add contacts.");
            return;
        }
        Scanner scan = new Scanner(System.in);
        System.out.println("What are you going to change? \n 1. number \n 2. home number \n " +
                "3. company \n 4. address \n 5. email \n 6. nothing");
        int change;
        try {
            change = scan.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Such command doesn't exist");
            return;
        }
        if (change < 1 || change > 5) {
            if (change != 6) {
                System.out.println("You chose wrong command. Try again");
            }
            return;
        }
        System.out.println("Write contact's name");
        String name = scan.next();
        NoteObject changeObj = NoteObject.values()[change - 1];
        System.out.println("Write changes, please.");
        String changes = scan.next();
        if (app.getStorage().change(name, changeObj, changes)) {
            app.getDataBaseConnection().changeContactInDataBase(name, changeObj, changes);
            System.out.println("Success!");
        } else {
            System.out.println("Warn! Contact wasn't found.");
        }
    }

    @Override
    public String name() {
        return "Change contact";
    }
}

