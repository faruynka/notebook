package notebook;

interface ICommand {
    void execute(NoteBookApp app);
    String name();
}
