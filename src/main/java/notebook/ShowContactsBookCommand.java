package notebook;

import myhashmap.IMapIterator;

class ShowContactsBookCommand implements ICommand {
    @Override
    public void execute(NoteBookApp app) {
        if (app.getStorage().getNoteBook().size() == 0) {
            System.out.println("Your notebook is empty. Choose first command to add contacts.");
            return;
        }
        IMapIterator iterator = app.getStorage().getNoteBook().iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next().getFirst());
        }
    }

    @Override
    public String name() {
        return "Show all contacts";
    }
}
