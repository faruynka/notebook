package notebook;

import java.io.Console;
import java.util.Scanner;

class Login {
    public static String getUserName() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter your username");
        return scan.next();
    }

    public static char[] getPassword() {
        char[] password;
        if (isRunningFromIntelliJ()) {
            Scanner scan = new Scanner(System.in);
            System.out.println("Please enter your password");
            password = scan.next().toCharArray();
        } else {
            Console console = System.console();
            password = console.readPassword("Please enter your password ");
        }
        return password;
    }

    private static boolean isRunningFromIntelliJ() {
        String classPath = System.getProperty("java.class.path");
        return classPath.contains("idea_rt.jar");
    }
}
