package notebook;

import java.util.Scanner;

class RemoveCommand implements ICommand {
    @Override
    public void execute(NoteBookApp app) {
        if (app.getStorage().getNoteBook().size() == 0) {
            System.out.println("Your notebook is empty. Choose first command to add contacts.");
            return;
        }
        Scanner scan = new Scanner(System.in);
        System.out.println("Write name");
        String name = scan.next();
        if (app.getStorage().remove(name)) {
            app.getDataBaseConnection().deleteContactInDataBase(name);
            System.out.println("Success!");
        } else {
            System.out.println("Warn! Contact wasn't found.");
        }
    }

    @Override
    public String name() {
        return "Delete contact";
    }
}
