package notebook;

import static notebook.Login.*;

class ChangeUserCommand implements ICommand {
    @Override
    public void execute(NoteBookApp app) {
        app.getSender().setUsername(getUserName());
    }

    @Override
    public String name() {
        return "Change user";
    }
}
