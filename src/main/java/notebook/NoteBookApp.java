package notebook;


import database.IDataBaseService;
import myhashmap.IMap;
import myhashmap.IMapIterator;
import myhashmap.MyHashMap;
import myhashmap.MyPair;
import notesstorage.INotesStorage;
import sender.ISender;

import java.util.InputMismatchException;
import java.util.Scanner;

public class NoteBookApp {
    private INotesStorage storage;
    private IMap<Integer, ICommand> commandsMap;
    private ISender sender;
    private IDataBaseService dataBaseConnection;

    public NoteBookApp(INotesStorage storage, ISender sender, IDataBaseService dataBaseConnection) {
        this.storage = storage;
        this.dataBaseConnection = dataBaseConnection;
        storage.setNoteBook(dataBaseConnection.importData());
        this.sender = sender;
        commandsMap = new MyHashMap();
        commandsMap.insert(1, new AddCommand());
        commandsMap.insert(2, new RemoveCommand());
        commandsMap.insert(3, new ChangeCommand());
        commandsMap.insert(4, new ShowContactsBookCommand());
        commandsMap.insert(5, new ShowContactCommand());
        commandsMap.insert(6, new FindContactsByPattern());
        commandsMap.insert(7, new SendContactCommand());
        commandsMap.insert(8, new ChangeUserCommand());
    }

    public void run() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Hello!");
        int command;
        for (; ; ) {
            try {
                IMapIterator iterator = commandsMap.iterator();
                System.out.println("Choose your command and enter it's number");
                while (iterator.hasNext()) {
                    MyPair<Integer, ICommand> commandPair = iterator.next();
                    System.out.println(commandPair.getFirst() + ". " + commandPair.getSecond().name());
                }
                System.out.println(commandsMap.size() + 1 + ". Exit");
                command = scan.nextInt();
                if (command < 1 || command > commandsMap.size() + 1) {
                    System.out.println("Command with this number does not exists!");
                    continue;
                }
                if (command == commandsMap.size() + 1) {
                    dataBaseConnection.closeConnection();
                    break;
                }
                commandsMap.find(command).execute(this);
            } catch (InputMismatchException e) {
                System.out.println("Input is incorrect!");
                scan.next();
            }
        }
    }

    ISender getSender() {
        return sender;
    }

    INotesStorage getStorage() {
        return storage;
    }

    IDataBaseService getDataBaseConnection() {
        return dataBaseConnection;
    }
}
