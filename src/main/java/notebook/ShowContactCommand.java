package notebook;

import notesstorage.INotesStorage;
import notesstorage.Note;

import java.util.Scanner;

class ShowContactCommand implements ICommand {
    @Override
    public void execute(NoteBookApp app) {
        if (app.getStorage().getNoteBook().size() == 0) {
            System.out.println("Your notebook is empty. Choose first command to add contacts.");
            return;
        }
        INotesStorage storage = app.getStorage();
        Scanner scan = new Scanner(System.in);
        System.out.println("Write name");
        String name = scan.next();
        Note note = storage.find(name);
        if (note != null) {
            System.out.println(name + "\nNumber: " + note.number() + "\nHomeNumber: " + note.homeNumber()
                    + "\nCompany: " + note.company() + "\nAddress: " + note.address() + "\nEmail: " + note.email());
        } else {
            System.out.println("Contact with this name wasn't found.");
        }
    }

    @Override
    public String name() {
        return "Show information about contact";
    }
}
