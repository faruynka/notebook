package notebook;

import notesstorage.Note;

import java.util.Scanner;

class AddCommand implements ICommand {
    @Override
    public void execute(NoteBookApp app) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Write name");
        String name = scan.next();
        System.out.println("Write number");
        String number = scan.next();
        System.out.println("Write homeNumber");
        String homeNumber = scan.next();
        System.out.println("Write company");
        String company = scan.next();
        System.out.println("Write address");
        String address = scan.next();
        System.out.println("Write email");
        String email = scan.next();
        Note note = new Note(number, homeNumber, company, address, email);
        if (app.getStorage().add(name, note)) {
            app.getDataBaseConnection().addContactIntoDataBase(name, note);
            System.out.println("Success!");
        } else {
            System.out.println("Warn! Contact with this name already exists.");
        }
    }

    @Override
    public String name() {
        return "Add contact";
    }
}
