package notebook;

import notesstorage.Note;
import sender.ISender;

import java.util.Scanner;

import static notebook.Login.*;

class SendContactCommand implements ICommand {
    @Override
    public void execute(NoteBookApp app) {
        if (app.getStorage().getNoteBook().size() == 0) {
            System.out.println("Your notebook is empty. Choose first command to add contacts.");
            return;
        }
        Scanner scan = new Scanner(System.in);
        System.out.println("Write contact you want to send");
        String contactName = scan.next();
        Note note = app.getStorage().find(contactName);
        if (note == null) {
            System.out.println("Contact wasn't found");
            return;
        }
        ISender sender = app.getSender();
        if (!sender.isLogIn()) {
            sender.setUsername(getUserName());
        }
        System.out.println("Write email you want to send contact on");
        String email = scan.next();
        try {
            sender.send(email, getPassword(), contactName, note);
            System.out.println("Done");
        } catch (RuntimeException e) {
            System.out.println("Email or password is incorrect!");
        }
    }

    @Override
    public String name() {
        return "Send contact on email";
    }
}
