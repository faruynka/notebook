import database.hibernateservice.HibernateService;
import notebook.NoteBookApp;
import notesstorage.NotesStorage;
import sender.EmailSender;

class Main {
    public static void main(String[] args) {
        NoteBookApp nb = new NoteBookApp(new NotesStorage(), new EmailSender(), new HibernateService());
        nb.run();
    }
}