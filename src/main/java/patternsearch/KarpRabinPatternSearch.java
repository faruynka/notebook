package patternsearch;

import java.util.ArrayList;
import java.util.List;

public class KarpRabinPatternSearch implements ITextPatternSearch {
    @Override
    public List<Integer> findPattern(char[] pattern, char[] text) {
        List<Integer> result = new ArrayList();
        if (pattern.length == 0) {
            return result;
        }
        int q = 17;
        int x = 2;
        int[] powerX = new int[pattern.length];
        powerX[0] = 1;
        for (int i = 0; i < pattern.length - 1; i++) {
            powerX[i + 1] = mod(powerX[i] * x, q);
        }
        int window = hashFunction(pattern, powerX, q, pattern.length);
        int hash = 0;
        for (int i = pattern.length - 1; i < text.length; i++) {
            if (i == pattern.length - 1) {
                hash = hashFunction(text, powerX, q, pattern.length);
            } else {
                hash = mod(mod(mod(hash - mod(mod(text[i - pattern.length], q)
                        * powerX[pattern.length - 1], q), q) * x, q) + mod(text[i], q), q);
            }
            if (hash == window) {
                boolean isDifference = false;
                for (int j = 0; j < pattern.length; j++) {
                    if (text[i + 1 - pattern.length + j] != pattern[j]) {
                        isDifference = true;
                        break;
                    }
                }
                if (!isDifference) {
                    result.add(i - pattern.length + 1);
                }
            }
        }
        return result;
    }

    private int mod(int number, int q) {
        int tmp = ((number % q) + q) % q;
        return tmp;
    }

    private int hashFunction(char[] str, int[] powerX, int q, int length) {
        int sum = 0;
        for (int i = length - 1; i >= 0; i--) {
            sum += mod(str[i] * powerX[length - 1 - i], q);
        }
        return mod(sum, q);
    }
}
