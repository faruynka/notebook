package patternsearch;

import java.util.List;

public interface ITextPatternSearch {
    List<Integer> findPattern(char[] pattern, char[] text);
}
