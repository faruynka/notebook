package sender;

import notesstorage.Note;

import java.util.Arrays;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailSender implements ISender {
    private String username;
    private Session session;

    @Override
    public void send(String userNameGeter, char[] password, String contactName, Note contactNote) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        this.session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, new String(password));
                    }
                });

        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(userNameGeter));
            message.setSubject("Sending contact");
            message.setText(contactName + "\nNumber: " + contactNote.number() + "\nHomeNumber: "
                    + contactNote.homeNumber() + "\nCompany: " + contactNote.company()
                    + "\nAddress: " + contactNote.address() + "\nEmail: " + contactNote.email());
            Transport.send(message);
            Arrays.fill(password, '0');
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isLogIn() {
        return username != null;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }
}