package sender;

import notesstorage.Note;

public interface ISender {
    public void send(String userNameGeter, char[] password, String contactName, Note contactNote);
    public void setUsername(String username);
    boolean isLogIn();
}
