package notesstorage;

public enum NoteObject {
    NUMBER,
    HOMENUMBER,
    COMPANY,
    ADDRESS,
    EMAIL
}
