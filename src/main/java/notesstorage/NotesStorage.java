package notesstorage;

import myhashmap.IMapIterator;
import myhashmap.MyPair;
import patternsearch.ITextPatternSearch;
import patternsearch.KarpRabinPatternSearch;
import myhashmap.IMap;
import myhashmap.MyHashMap;

import java.util.ArrayList;
import java.util.List;

public class NotesStorage implements INotesStorage {
    private IMap<String, Note> noteBook;
    private ITextPatternSearch patternSearch;

    public NotesStorage(IMap<String, Note> noteBook, ITextPatternSearch patternSearch) {
        this.noteBook = noteBook;
        this.patternSearch = patternSearch;
    }

    public NotesStorage() {
        this(new MyHashMap(), new KarpRabinPatternSearch());
    }


    @Override
    public boolean add(String name, Note note) {
        return noteBook.insert(name, note);
    }

    @Override
    public boolean remove(String name) {
        return noteBook.remove(name);
    }

    @Override
    public boolean change(String name, NoteObject changeNoteObject, String newNoteObject) {
        Note oldNoteObj = noteBook.find(name);
        if (oldNoteObj == null) {
            return false;
        }
        noteBook.remove(name);
        if (changeNoteObject.equals(NoteObject.NUMBER)) {
            oldNoteObj.setNumber(newNoteObject);
        } else if (changeNoteObject.equals(NoteObject.HOMENUMBER)) {
            oldNoteObj.setHomeNumber(newNoteObject);
        } else if (changeNoteObject.equals(NoteObject.COMPANY)) {
            oldNoteObj.setCompany(newNoteObject);
        } else if (changeNoteObject.equals(NoteObject.ADDRESS)) {
            oldNoteObj.setAddress(newNoteObject);
        } else if (changeNoteObject.equals(NoteObject.EMAIL)) {
            oldNoteObj.setEmail(newNoteObject);
        }
        return noteBook.insert(name, oldNoteObj);
    }

    @Override
    public Note find(String name) {
        return noteBook.find(name);
    }

    @Override
    public void setNoteBook(IMap<String, Note> noteBook) {
        this.noteBook = noteBook;
    }

    @Override
    public IMap<String, Note> getNoteBook() {
        return noteBook;
    }

    @Override
    public List<String> findNotesWithPattern(String pattern) {
        IMapIterator iterator = noteBook.iterator();
        List<String> namesContainsPattern = new ArrayList();
        while (iterator.hasNext()) {
            MyPair<String, Note> mapObject = iterator.next();
            List searchRes = patternSearch.findPattern(pattern.toLowerCase().toCharArray(),
                    mapObject.getFirst().toLowerCase().toCharArray());
            if (!searchRes.isEmpty()) {
                namesContainsPattern.add(mapObject.getFirst());
            }
        }
        return namesContainsPattern;
    }
}
