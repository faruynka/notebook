package notesstorage;

public class Note {
    private String number;
    private String homeNumber;
    private String company;
    private String address;
    private String email;

    public Note(String number, String homeNumber, String company, String address, String email) {
        this.number = number;
        this.homeNumber = homeNumber;
        this.company = company;
        this.address = address;
        this.email = email;
    }

    public String number() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String homeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String company() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String address() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String email() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
