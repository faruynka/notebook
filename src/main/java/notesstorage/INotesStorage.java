package notesstorage;

import myhashmap.IMap;

import java.util.List;

public interface INotesStorage {
    boolean add(String name, Note note);

    boolean remove(String name);

    boolean change(String name, NoteObject changeNoteObject, String newNoteObject);

    Note find(String name);

    List<String> findNotesWithPattern(String pattern);

    IMap<String, Note> getNoteBook();

    void setNoteBook(IMap<String, Note> noteBook);
}

